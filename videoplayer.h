#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QMediaPlayer>
#include <QVideoWidget>

#include "video.h"

class VideoPlayer : public QVideoWidget {
	Q_OBJECT
  public:
	explicit VideoPlayer(QWidget* parent = nullptr);

	Video* getCurrentVideo() const { return currentVideo; }

  public slots:
	void playMedia(Video* video);
	void playStateChanged(QMediaPlayer::State ms);
	void videoPositionChanged(qint64 position);
	void timelinePositionChanged(qint64 position);
	void togglePlayState();
	void jumpForward();
	void jumpBackward();
	void jumpToHighlight(float);
	void highlightCreate();

  signals:
	void videoSelected();
	void durationChange(Video* video, qint64 duration);
	void videoProgressed(qint64 position);
	void highlightAdded(Video* video, qint64 duration);

  private:
	QMediaPlayer* mediaPlayer;

	Video* currentVideo = nullptr;
};

#endif  // VIDEOPLAYER_H
