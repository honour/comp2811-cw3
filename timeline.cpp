#include "timeline.h"

#include "video.h"

Timeline::Timeline(Qt::Orientation orientation, QWidget* parent) : QSlider(orientation, parent) {}

void Timeline::videoChanged() {
	setMinimum(0);
	setValue(0);
}

void Timeline::durationChanged(Video* video, qint64 duration) { setMaximum(duration / 10); }

void Timeline::timelineChanged(qint64 position) { setValue(position / 10); }
