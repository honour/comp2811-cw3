#include "categorydialog.h"

#include <QComboBox>
#include <QInputDialog>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include "category.h"

CategoryDialog::CategoryDialog(std::vector<Category*>* categories) : QDialog() {
	setMinimumHeight(100);
	setMinimumWidth(200);

	inputPrompt = new QInputDialog(this);
	inputPrompt->setLabelText("Enter the new category name:");

	auto layout = new QVBoxLayout(this);
	auto label = new QLabel("Select the category to add the video to", this);
	comboBox = new QComboBox(this);

	auto btnRow = new QWidget(this);
	auto buttonLayout = new QHBoxLayout(this);

	auto submitBtn = new QPushButton("Submit", this);
	auto cancelBtn = new QPushButton("Cancel", this);

	auto newBtn = new QPushButton("New category", this);

	buttonLayout->addWidget(cancelBtn);
	buttonLayout->addWidget(submitBtn);
	btnRow->setLayout(buttonLayout);

	buttonLayout->addWidget(cancelBtn);
	buttonLayout->addWidget(submitBtn);

	setCategories(categories);

	layout->addWidget(label);
	layout->addWidget(comboBox);
	layout->addWidget(newBtn);
	layout->addWidget(btnRow);

	setLayout(layout);

	connect(submitBtn, &QPushButton::released, this, [&] {
		emit categorySelected(comboBox->currentText());
		close();
	});
	connect(inputPrompt, &QInputDialog::textValueSelected, this, &CategoryDialog::newCategory);
	connect(newBtn, &QPushButton::released, inputPrompt, &QDialog::show);
	connect(cancelBtn, &QPushButton::released, this, &QDialog::close);
}

void CategoryDialog::setCategories(std::vector<Category*>* categories) {
	_categories = categories;
	comboBox->clear();
	for (const auto& category : *_categories) {
		comboBox->addItem(category->getName());
	}
}

void CategoryDialog::newCategory(const QString category) {
	comboBox->addItem(category);
	// Set index to last index (the one we just added)
	comboBox->setCurrentIndex(comboBox->count() - 1);
	auto newCategory = new Category(category);
	_categories->insert(_categories->begin(), newCategory);
}
