#ifndef TIMELINE_H
#define TIMELINE_H

#include <QSlider>

#include "video.h"

class Timeline : public QSlider {
	Q_OBJECT
  public:
	explicit Timeline(Qt::Orientation o, QWidget* parent);

  public slots:
	void timelineChanged(qint64 position);
	void videoChanged();
	void durationChanged(Video* video, qint64 duration);
};

#endif  // TIMELINE_H
