#include "category.h"

#include "video.h"

void Category::addVideo(Video *video) {
	if (!containsVideo(video)) {
		videos.push_back(video);
	}
}

bool Category::removeVideo(Video *video) {
	if (containsVideo(video)) {
		for (auto it = videos.begin(); it != videos.end(); it++) {
			if (*it == video) {
				videos.erase(it);
				return true;
			}
		}
	}
	return false;
}

bool Category::containsVideo(Video *video) { return std::find(videos.begin(), videos.end(), video) != videos.end(); }
