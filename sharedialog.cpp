#include "sharedialog.h"

#include <QInputDialog>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

ShareDialog::ShareDialog() : QDialog() {
	setMinimumHeight(100);
	setMinimumWidth(200);

	auto layout = new QVBoxLayout(this);

	auto label = new QLabel("Uploading...");
	auto btnRow = new QWidget(this);
	auto buttonLayout = new QHBoxLayout(this);

	auto submitBtn = new QPushButton("OK", this);
	auto cancelBtn = new QPushButton("Cancel", this);

	label->setAlignment(Qt::AlignCenter);

	buttonLayout->addWidget(cancelBtn);
	buttonLayout->addWidget(submitBtn);
	btnRow->setLayout(buttonLayout);

	buttonLayout->addWidget(cancelBtn);
	buttonLayout->addWidget(submitBtn);

	layout->addWidget(label);
	layout->addWidget(btnRow);

	setLayout(layout);

	connect(submitBtn, &QPushButton::released, this, [&] { close(); });
	connect(cancelBtn, &QPushButton::released, this, &QDialog::close);
}
