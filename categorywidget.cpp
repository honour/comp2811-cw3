#include "categorywidget.h"

CategoryWidget::CategoryWidget(Category *category, QWidget *parent) : QWidget(parent) {
	title = new QLabel(this);
	title->setText(category->getName());

	grid = new ThumbnailGrid(category->getVideos());

	layout = new QVBoxLayout(this);

	layout->addWidget(title);
	layout->addWidget(grid);

	setLayout(layout);
}
