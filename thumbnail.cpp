#include "thumbnail.h"

#include <QFile>
#include <QFileInfo>

ThumbnailGrid::ThumbnailGrid(const std::vector<Video *> &videos, QWidget *parent) : QWidget(parent) {
	layout = new QGridLayout(this);
	layout->setMargin(2);

	const int columns = 2;
	int vidCount = 0;
	for (auto vid : videos) {
		auto button = new Thumbnail(this);
		button->init(vid);

		buttons.push_back(button);

		layout->addWidget(button, vidCount / columns, vidCount % columns);
		vidCount++;

		connect(button, &Thumbnail::selected, this, &ThumbnailGrid::selectVideo);
	}

	setLayout(layout);
}

void ThumbnailGrid::selectVideo(Video *info) { emit videoSelected(info); }

void Thumbnail::init(Video *i) {
	info = i;
	setIcon(*(i->getIcon()));
	setText(i->getTitle());
}

void Thumbnail::clicked() { emit selected(info); }
