#ifndef CATEGORYWIDGET_H
#define CATEGORYWIDGET_H

#include <QLabel>
#include <QVBoxLayout>
#include <QWidget>

#include "category.h"
#include "thumbnail.h"

class CategoryWidget : public QWidget {
  public:
	CategoryWidget(Category* category, QWidget* parent = nullptr);

	const ThumbnailGrid* getThumbnailGrid() const { return grid; }

  private:
	QLabel* title;
	ThumbnailGrid* grid;
	QVBoxLayout* layout;
};

#endif  // CATEGORYWIDGET_H
