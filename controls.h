#ifndef CONTROLS_H
#define CONTROLS_H

#include <QLayout>
#include <QPushButton>
#include <QWidget>

class ControlButton : public QPushButton {
	Q_OBJECT
  public:
	explicit ControlButton(QWidget* parent) : QPushButton(parent) {
		setMaximumSize(40, 40);
		setIconSize(QSize(32, 32));
	}
};

class Controls : public QWidget {
	Q_OBJECT
  public:
	explicit Controls(QWidget* parent);

  private:
	QHBoxLayout* layout;
	ControlButton* playButton;
	ControlButton* nextButton;
	ControlButton* prevButton;
	ControlButton* categoriesButton;
	ControlButton* highlightButton;
	ControlButton* shareButton;
	bool playing = true;

  private slots:
	void playButtonClicked();
	void nextButtonClicked();
	void prevButtonClicked();
	void categoryButtonClicked();
	void highlightButtonClicked();
	void shareButtonClicked();

  public slots:
	void videoPlaying();

  signals:
	void playButtonPressed();
	void nextButtonPressed();
	void prevButtonPressed();
	void categoryButtonPressed();
	void highlightButtonPressed();
};

#endif  // CONTROLS_H
