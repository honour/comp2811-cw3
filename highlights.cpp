#include "highlights.h"

#include <QLabel>
#include <QLayout>
#include <QWidget>

#include "highlight_button.h"
#include "highlights_layout.h"

Highlights::Highlights(QWidget* parent) : QWidget(parent) {
	auto l = new HighlightsLayout;
	setLayout(l);
}

void Highlights::videoChanged() {
	delete layout();
	qDeleteAll(children());

	auto l = new HighlightsLayout;
	setLayout(l);
}

void Highlights::durationChanged(Video* video, qint64 duration) {
	auto scale = width() / (float)duration;

	for (auto h : video->highlights) {
		auto button = new HighlightButton(this, h);
		button->setIcon(QIcon(":/img/highlight.png"));
		button->setGeometry((scale * h) - 12, 0, 24, 24);

		auto l = layout();
		l->addWidget(button);
		setLayout(l);

		connect(button, &HighlightButton::selected, this, &Highlights::highlightSelected);
	}
}

void Highlights::highlightsUpdated(Video* video, qint64 duration) {
	auto scale = width() / (float)duration;
	auto size = video->highlights.size();
	auto highlight = video->highlights.at(size - 1);

	auto button = new HighlightButton(this, highlight);
	button->setIcon(QIcon(":/img/highlight.png"));
	button->setGeometry((scale * highlight) - 12, 0, 24, 24);

	auto l = layout();
	l->addWidget(button);
	setLayout(l);

	connect(button, &HighlightButton::selected, this, &Highlights::highlightSelected);
}
