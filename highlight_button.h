#ifndef HIGHLIGHT_BUTTON_H
#define HIGHLIGHT_BUTTON_H

#include <QPushButton>

class HighlightButton : public QPushButton {
	Q_OBJECT

  public:
	HighlightButton(QWidget *parent, qint64 position) : QPushButton(parent) {
		setPosition(position);
		connect(this, SIGNAL(released()), this, SLOT(clicked()));
	}

	void setPosition(float p) { position = p; }

  private:
	float position;

  public slots:
	void clicked();

  signals:
	void selected(float);
};

#endif  // HIGHLIGHT_BUTTON_H
