//
// Created by twak on 11/11/2019.
//

#ifndef VIDEO_H
#define VIDEO_H

#include <QFile>
#include <QFileInfo>
#include <QPushButton>
#include <QUrl>

class Video {
  public:
	Video(QUrl *url, QIcon *icon) : url(url), icon(icon) {
		auto localFile = url->toLocalFile();
		QFile file(localFile);
		QFileInfo info(file);
		title = info.baseName();
	}

	~Video() {
		delete url;
		delete icon;
	}

	QUrl *getURL() const { return url; }
	QIcon *getIcon() const { return icon; }
	QString getTitle() const { return title; }

	std::vector<qint64> highlights;

  private:
	QUrl *url;
	QIcon *icon;
	QString title;
};

#endif  // VIDEO_H
