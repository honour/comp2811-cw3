#ifndef HIGHLIGHTSLAYOUT_H
#define HIGHLIGHTSLAYOUT_H

#include <QLayout>
#include <QList>
#include <QtGui>

class HighlightsLayout : public QLayout {
  public:
	HighlightsLayout() : QLayout() {}
	~HighlightsLayout();

	void setGeometry(const QRect &rect);

	void addItem(QLayoutItem *item);
	QSize sizeHint() const;
	QSize minimumSize() const;
	int count() const;
	QLayoutItem *itemAt(int) const;
	QLayoutItem *takeAt(int);

  private:
	QList<QLayoutItem *> list_;
};
#endif  // HIGHLIGHTSLAYOUT_H
