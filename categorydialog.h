#ifndef CATEGORYDIALOG_H
#define CATEGORYDIALOG_H

#include <QComboBox>
#include <QDialog>
#include <QInputDialog>
#include <QWidget>

#include "category.h"

class CategoryDialog : public QDialog {
	Q_OBJECT
  public:
	CategoryDialog(std::vector<Category *> *categories);

	void setCategories(std::vector<Category *> *categories);

  private:
	QComboBox *comboBox;
	QInputDialog *inputPrompt;
	std::vector<Category *> *_categories;

  signals:
	void categorySelected(const QString category);

  private slots:
	void newCategory(const QString category);
};

#endif  // CATEGORYDIALOG_H
