#ifndef THUMBNAIL_H
#define THUMBNAIL_H

#include <QGridLayout>
#include <QToolButton>
#include <QWidget>

#include "video.h"

class Thumbnail : public QToolButton {
	Q_OBJECT

  public:
	Thumbnail(QWidget *parent) : QToolButton(parent) {
		setIconSize(QSize(140, 90));
		setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

		connect(this, SIGNAL(released()), this, SLOT(clicked()));
	}

	void init(Video *i);

	Video *getInfo() const { return info; }

  private:
	Video *info;

  public slots:
	void clicked();

  signals:
	void selected(Video *);
};

class ThumbnailGrid : public QWidget {
	Q_OBJECT
  public:
	explicit ThumbnailGrid(const std::vector<Video *> &videos, QWidget *parent = nullptr);

  private:
	std::vector<Thumbnail *> buttons;

	QGridLayout *layout;

  private slots:
	void selectVideo(Video *);

  signals:
	void videoSelected(Video *);
};

#endif  // THUMBNAIL_H
