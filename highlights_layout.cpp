#include "highlights_layout.h"

#include <math.h>

#include <QScrollArea>
#include <iostream>

void HighlightsLayout::setGeometry(const QRect &r) { QLayout::setGeometry(r); }

int HighlightsLayout::count() const { return list_.size(); }

QLayoutItem *HighlightsLayout::itemAt(int idx) const { return list_.value(idx); }

QLayoutItem *HighlightsLayout::takeAt(int idx) { return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0; }

void HighlightsLayout::addItem(QLayoutItem *item) { list_.append(item); }

QSize HighlightsLayout::sizeHint() const { return minimumSize(); }

QSize HighlightsLayout::minimumSize() const { return QSize(24, 24); }

HighlightsLayout::~HighlightsLayout() {
	QLayoutItem *item;
	while ((item = takeAt(0))) delete item;
}
