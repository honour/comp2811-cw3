#include "videoplayer.h"

#include <iostream>

VideoPlayer::VideoPlayer(QWidget* parent) : QVideoWidget(parent) {
	mediaPlayer = new QMediaPlayer(this);
	mediaPlayer->setVideoOutput(this);
	mediaPlayer->setNotifyInterval(25);

	connect(mediaPlayer, &QMediaPlayer::stateChanged, this, &VideoPlayer::playStateChanged);
	connect(mediaPlayer, &QMediaPlayer::positionChanged, this, &VideoPlayer::videoPositionChanged);
	connect(mediaPlayer, &QMediaPlayer::durationChanged, this,
	        [&](qint64 dur) { emit durationChange(currentVideo, dur); });
}

void VideoPlayer::playMedia(Video* video) {
	if (video == nullptr) return;

	mediaPlayer->setMedia(*video->getURL());
	mediaPlayer->play();

	currentVideo = video;

	emit videoSelected();
}

void VideoPlayer::playStateChanged(QMediaPlayer::State state) {
	if (state == QMediaPlayer::State::StoppedState) {
		mediaPlayer->play();
	}
}

void VideoPlayer::timelinePositionChanged(qint64 position) { mediaPlayer->setPosition(position); }

void VideoPlayer::togglePlayState() {
	if (mediaPlayer->state() == QMediaPlayer::State::PlayingState)
		mediaPlayer->pause();
	else
		mediaPlayer->play();
}

void VideoPlayer::videoPositionChanged(qint64 position) { emit videoProgressed(position); }

void VideoPlayer::jumpForward() {
	auto currentPosition = mediaPlayer->position();
	auto duration = mediaPlayer->duration();

	qint64 newPosition;

	if (currentPosition + 10000 >= duration)
		newPosition = 0;
	else
		newPosition = currentPosition + 10000;

	mediaPlayer->setPosition(newPosition);
}

void VideoPlayer::jumpBackward() {
	auto currentPosition = mediaPlayer->position();

	qint64 newPosition;

	if (currentPosition - 10000 <= 0)
		newPosition = 0;
	else
		newPosition = currentPosition - 10000;

	mediaPlayer->setPosition(newPosition);
}

void VideoPlayer::highlightCreate() {
	auto position = mediaPlayer->position();

	currentVideo->highlights.push_back(position);

	emit highlightAdded(currentVideo, mediaPlayer->duration());
}

void VideoPlayer::jumpToHighlight(float position) { mediaPlayer->setPosition(position); }
