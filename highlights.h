#ifndef HIGHLIGHTS_H
#define HIGHLIGHTS_H

#include <QWidget>

#include "video.h"

class Highlights : public QWidget {
	Q_OBJECT
  public:
	explicit Highlights(QWidget* parent);

  private:
	void addHighlights(Video* video, float scale);

  public slots:
	void highlightsUpdated(Video* video, qint64 duration);
	void durationChanged(Video* video, qint64 duration);
	void highlightSelected(qint64 position) { emit highlightClicked(position); }
	void videoChanged();

  signals:
	void highlightClicked(float);
};

#endif  // HIGHLIGHTS_H
