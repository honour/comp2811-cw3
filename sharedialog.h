#ifndef SHAREDIALOG_H
#define SHAREDIALOG_H

#include <QComboBox>
#include <QDialog>
#include <QInputDialog>
#include <QWidget>

class ShareDialog : public QDialog {
	Q_OBJECT
  public:
	ShareDialog();

  private:
	QInputDialog *inputPrompt;
};

#endif  // SHAREDIALOG_H
