#include "controls.h"

#include <QLayout>
#include <QPushButton>

#include "sharedialog.h"
#include "videoplayer.h"

Controls::Controls(QWidget* parent) : QWidget(parent) {
	layout = new QHBoxLayout(this);

	playButton = new ControlButton(this);
	prevButton = new ControlButton(this);
	nextButton = new ControlButton(this);
	categoriesButton = new ControlButton(this);
	highlightButton = new ControlButton(this);
	shareButton = new ControlButton(this);

	playButton->setIcon(QIcon(":/img/pause.png"));
	prevButton->setIcon(QIcon(":/img/prev.png"));
	nextButton->setIcon(QIcon(":/img/next.png"));
	categoriesButton->setIcon(QIcon(":/img/categories.png"));
	highlightButton->setIcon(QIcon(":/img/highlight.png"));
	shareButton->setIcon(QIcon(":/img/share.png"));

	layout->setMargin(8);
	layout->setSpacing(0);

	auto widget = new QWidget;
	widget->setMaximumWidth(72);

	layout->addWidget(prevButton);
	layout->addWidget(playButton);
	layout->addWidget(nextButton);
	layout->addWidget(widget);
	layout->addWidget(shareButton);
	layout->addWidget(categoriesButton);
	layout->addWidget(highlightButton);

	connect(playButton, &QPushButton::released, this, &Controls::playButtonClicked);
	connect(prevButton, &QPushButton::released, this, &Controls::nextButtonClicked);
	connect(nextButton, &QPushButton::released, this, &Controls::prevButtonClicked);
	connect(categoriesButton, &QPushButton::released, this, &Controls::categoryButtonClicked);
	connect(highlightButton, &QPushButton::released, this, &Controls::highlightButtonClicked);
	connect(shareButton, &QPushButton::released, this, &Controls::shareButtonClicked);

	setLayout(layout);
}

void Controls::playButtonClicked() {
	if (playing)
		playButton->setIcon(QIcon(":/img/play.png"));
	else
		playButton->setIcon(QIcon(":/img/pause.png"));

	playing = !playing;
	emit playButtonPressed();
}

void Controls::shareButtonClicked() {
	auto dialog = new ShareDialog();
	dialog->show();
}

void Controls::prevButtonClicked() { emit prevButtonPressed(); }

void Controls::nextButtonClicked() { emit nextButtonPressed(); }

void Controls::categoryButtonClicked() { emit categoryButtonPressed(); }

void Controls::highlightButtonClicked() { emit highlightButtonPressed(); }

void Controls::videoPlaying() {
	playButton->setIcon(QIcon(":/img/pause.png"));
	playing = true;
}
