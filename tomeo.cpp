#include "tomeo.h"

#include <QDialog>
#include <QDirIterator>
#include <QImageReader>
#include <QInputDialog>
#include <QLabel>
#include <QLayout>

#include "categorydialog.h"
#include "categorywidget.h"
#include "controls.h"

const int WIDTH = 380;
const int HEIGHT = 720;

Tomeo::Tomeo(const QDir dir, QWidget *parent) : QWidget(parent) {
	player = new VideoPlayer(this);
	layout = new QVBoxLayout(this);
	highlights = new Highlights(this);
	controls = new Controls(this);
	timeline = new Timeline(Qt::Horizontal, this);

	setVideoDirectory(dir);

	// Set up default category
	auto defaultCategory = new Category("Uncategorised");
	categories.push_back(defaultCategory);

	setUpDefaultCategory(defaultCategory);

	scrollArea = new QScrollArea();
	refreshCategoryWidgets();

	scrollArea->setMaximumHeight(0.5125 * HEIGHT);
	scrollArea->setMaximumWidth(WIDTH);
	scrollArea->setAlignment(Qt::AlignHCenter);

	highlights->setMaximumHeight(24);
	highlights->setMaximumWidth(WIDTH);

	controls->setMaximumHeight(48);
	controls->setMaximumWidth(WIDTH);

	timeline->setMaximumHeight(48);
	timeline->setMaximumWidth(WIDTH);

	layout->setSpacing(4);
	layout->setMargin(8);
	layout->addWidget(player);
	layout->addWidget(highlights);
	layout->addWidget(timeline);
	layout->addWidget(controls);
	layout->addWidget(scrollArea);
	setLayout(layout);

	setWindowTitle("Tomeo");
	setMinimumSize(WIDTH, HEIGHT);
	setMaximumSize(WIDTH, HEIGHT);

	auto vid = *videos.begin();
	player->playMedia(vid);

	connect(controls, &Controls::playButtonPressed, player, &VideoPlayer::togglePlayState);
	connect(controls, &Controls::prevButtonPressed, player, &VideoPlayer::jumpForward);
	connect(controls, &Controls::nextButtonPressed, player, &VideoPlayer::jumpBackward);
	connect(controls, &Controls::highlightButtonPressed, player, &VideoPlayer::highlightCreate);
	connect(controls, &Controls::categoryButtonPressed, this, &Tomeo::categoryDialog);
	connect(player, &VideoPlayer::videoSelected, controls, &Controls::videoPlaying);
	connect(player, &VideoPlayer::videoSelected, timeline, &Timeline::videoChanged);
	connect(player, &VideoPlayer::videoSelected, highlights, &Highlights::videoChanged);
	connect(player, &VideoPlayer::durationChange, timeline, &Timeline::durationChanged);
	connect(player, &VideoPlayer::durationChange, highlights, &Highlights::durationChanged);
	connect(player, &VideoPlayer::videoProgressed, timeline, &Timeline::timelineChanged);
	connect(player, &VideoPlayer::highlightAdded, highlights, &Highlights::highlightsUpdated);
	connect(highlights, &Highlights::highlightClicked, player, &VideoPlayer::jumpToHighlight);
}

Tomeo::~Tomeo() {
	for (auto vid : videos) {
		delete vid;
	}
	delete scrollArea;
}

void Tomeo::categoryDialog() {
	auto dialog = new CategoryDialog(&categories);
	dialog->show();

	connect(dialog, &CategoryDialog::categorySelected, this, &Tomeo::setCurrentVideoCategory);
}

void Tomeo::refreshCategoryWidgets() {
	auto newCategoryArea = new QWidget(this);
	auto newLayout = new QVBoxLayout();
	for (auto category : categories) {
		auto categoryWidget = new CategoryWidget(category);
		newLayout->addWidget(categoryWidget);

		auto thumbnailGrid = categoryWidget->getThumbnailGrid();
		connect(thumbnailGrid, &ThumbnailGrid::videoSelected, player, &VideoPlayer::playMedia);
	}

	newCategoryArea->setLayout(newLayout);
	newCategoryArea->setMaximumWidth(scrollArea->width() - 60);
	scrollArea->setWidget(newCategoryArea);

	categoryLayout = newLayout;
	categoryArea = newCategoryArea;
}

void Tomeo::setCurrentVideoCategory(const QString name) {
	// Search for the category
	Category *category = nullptr;
	for (auto it = categories.begin(); it != categories.end(); it++) {
		auto search = *it;
		if (search->getName() == name) {
			category = search;
			break;
		}
	}

	// We should have found the category
	if (category == nullptr) return;

	auto defaultCategory = *categories.begin();
	auto currentVideo = player->getCurrentVideo();
	if (currentVideo == nullptr) return;
	defaultCategory->removeVideo(currentVideo);

	category->addVideo(currentVideo);

	refreshCategoryWidgets();
}

void Tomeo::setVideoDirectory(const QDir dir) {
	videos.clear();

	QDirIterator it(dir);
	while (it.hasNext()) {
		QFile f(it.next());
		auto fileName = f.fileName();

		if (fileName.contains(".")) {
#if defined(_WIN32)
			if (fileName.contains(".wmv")) {  // windows
#else
			if (fileName.contains(".mp4") || fileName.contains("MOV")) {  // mac/linux
#endif
				auto thumbPath = fileName.left(fileName.length() - 4) + ".png";
				QFile thumb(thumbPath);

				if (thumb.exists()) {
					QImageReader *imageReader = new QImageReader(thumb.fileName());
					QImage sprite = imageReader->read();  // read the thumbnail
					if (!sprite.isNull()) {
						QIcon *ico = new QIcon(QPixmap::fromImage(sprite));  // voodoo to create an
						                                                     // icon for the button
						QUrl *url =
						    new QUrl(QUrl::fromLocalFile(fileName));  // convert the file location to a generic url
						auto video = new Video(url, ico);

						videos.push_back(video);
						videos.push_back(video);  // second just for testing
					} else {
						qDebug() << "coudln't read thumbnail for " << fileName;
					}
				} else {
					qDebug() << "skipping " << fileName << " because missing thumbnail";
				}
			}
		}
	}
}

// Adds videos to default category
void Tomeo::setUpDefaultCategory(Category *defaultCategory) {
	for (auto vid : videos) {
		defaultCategory->addVideo(vid);
	}
}
