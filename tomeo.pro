QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        category.cpp \
        categorydialog.cpp \
        categorywidget.cpp \
        controls.cpp \
        highlight_button.cpp \
        highlights.cpp \
        highlights_layout.cpp \
        main.cpp \
        sharedialog.cpp \
        thumbnail.cpp \
        timeline.cpp \
        tomeo.cpp \
        videoplayer.cpp

HEADERS += \
    category.h \
    categorydialog.h \
    categorywidget.h \
    controls.h \
    highlight_button.h \
    highlights.h \
    highlights_layout.h \
    sharedialog.h \
    thumbnail.h \
    timeline.h \
    tomeo.h \
    video.h \
    videoplayer.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    data/resources.qrc

