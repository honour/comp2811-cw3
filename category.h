#ifndef CATEGORY_H
#define CATEGORY_H

#include <algorithm>
#include <vector>

#include "video.h"

class Category {
  public:
	Category(QString name) : name(name) {}

  public:
	void addVideo(Video *video);
	bool removeVideo(Video *video);
	bool containsVideo(Video *video);

	std::vector<Video *> getVideos() const { return videos; }
	QString getName() const { return name; }

  private:
	QString name;
	std::vector<Video *> videos;
};

#endif  // CATEGORY_H
