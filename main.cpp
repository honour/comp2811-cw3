//    ______
//   /_  __/___  ____ ___  ___  ____
//    / / / __ \/ __ `__ \/ _ \/ __ \
//   / / / /_/ / / / / / /  __/ /_/ /
//  /_/  \____/_/ /_/ /_/\___/\____/
//              video for sports enthusiasts...
//
//  2811 cw3 : twak 11/11/2021
//

#include <QApplication>
#include <QDesktopServices>
#include <QImageReader>
#include <QMediaPlaylist>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtCore/QFileInfo>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QtWidgets/QFileIconProvider>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <iostream>
#include <string>
#include <vector>

#include "tomeo.h"
#include "video.h"

int main(int argc, char *argv[]) {
	// let's just check that Qt is operational first
	qDebug() << "Qt version: " << QT_VERSION_STR << endl;

	QApplication app(argc, argv);

	if (argc < 2) {
		QMessageBox messageBox;
		messageBox.critical(0, "Error", "Incorrect number of arguments.");
		exit(-1);
	}

	auto loc = QString::fromStdString(std::string(argv[1]));
	QDir dir(loc);

	if (!dir.exists()) {
		QMessageBox messageBox;
		messageBox.critical(0, "Invalid directory", "The video directory does not exist.");
		exit(-1);
	}

	QFile styleFile(":/styles.qss");
	styleFile.open(QFile::ReadOnly);
	QString style(styleFile.readAll());
	app.setStyleSheet(style);

	auto tomeo = new Tomeo(dir);
	tomeo->show();

	return app.exec();
}
