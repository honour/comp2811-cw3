#ifndef TOMEO_H
#define TOMEO_H

#include <QDir>
#include <QLayout>
#include <QScrollArea>
#include <QWidget>

#include "category.h"
#include "controls.h"
#include "highlights.h"
#include "thumbnail.h"
#include "timeline.h"
#include "video.h"
#include "videoplayer.h"

class Tomeo : public QWidget {
	Q_OBJECT
  public:
	Tomeo(const QDir dir, QWidget* parent = nullptr);
	~Tomeo();

  private:
	QVBoxLayout* layout;
	VideoPlayer* player;
	Timeline* timeline;
	Highlights* highlights;
	Controls* controls;
	ThumbnailGrid* thumbnailGrid;
	QWidget* categoryArea = nullptr;
	QVBoxLayout* categoryLayout = nullptr;
	QScrollArea* scrollArea;
	std::vector<Video*> videos;
	std::vector<Category*> categories;

	void setVideoDirectory(const QDir dir);
	void setUpDefaultCategory(Category*);
	void refreshCategoryWidgets();

  private slots:
	void categoryDialog();
	void setCurrentVideoCategory(const QString category);
};

#endif  // TOMEO_H
